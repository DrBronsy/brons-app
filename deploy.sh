docker-compose down
docker rmi $(docker images -q --filter "dangling=true")
docker-compose pull
docker-compose up -d --remove-orphans