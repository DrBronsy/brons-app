import * as React from 'react';
import * as moment from 'moment'
import {Typography, Avatar, Card, Col, Row, Space, Statistic} from 'antd';
import {UserOutlined, DollarCircleOutlined} from '@ant-design/icons';
import {block as bem} from 'bem-cn';
import cn from 'classnames';

import './index.scss';
import useGetMe from 'hooks/useGetMe';
import useGetScore from 'hooks/useGetScore';

const {Link, Text} = Typography;

const block = bem('profile-main-info');

export interface Props {
  className?: string
}

export default function ProfileProfileMainInfo({className}: Props): JSX.Element {
  const {loading, data} = useGetMe();
  const {loading: vkInfoLoading, data: scoreData} = useGetScore();

  if (loading || vkInfoLoading) return <div>Loading</div>;

  const {user: userScores} = scoreData;
  const {scores} = userScores;
  const {ebal} = scores;
  const {user} = data;
  const {csgoStat, steam, displayName} = user;
  const {playtime_2weeks, total_time_played, playtime_forever} = csgoStat;
  const {avatarfull, profileurl, timecreated} = steam;

  const styleCardBody = {
    width: '100%',
    display: 'flex'
  }

  const playtimeForever = Math.round(parseInt(playtime_forever, 0) / 60);
  const playtimeReal = Math.round(parseInt(total_time_played, 0) / 60 / 60);
  const playtime2weeks = Math.round(parseInt(playtime_2weeks, 0) / 60);

  return (
      <div className={cn(block(), className)}>
        <Row gutter={14}>
          <Col span={18}>
            <Card className={block('card')} bordered={false} bodyStyle={styleCardBody}>
              <Row gutter={24}>
                <Col flex='100px'>
                  <Avatar size={160} src={avatarfull} icon={<UserOutlined />} />
                </Col>
                <Col flex='auto'>
                  <Space direction='vertical'>
                    <Link href={profileurl} strong={true} target='_blank' style={{fontSize: '24px'}}>{displayName}</Link>
                    <Text type='secondary'>Дата создания: {moment(timecreated * 1000).format('DD.MM.YYYY')}</Text>
                    {!!playtimeForever && <Text type='secondary'>Количество общих часов в CSGO: {playtimeForever}</Text>}
                    {!!playtimeReal && <Text type='secondary'>Количество реальных часов в CSGO: {playtimeReal}</Text>}
                    {!!playtime2weeks && <Text type='secondary'>Количество часов за последние 2 недели в CSGO: {playtime2weeks}</Text>}
                  </Space>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col span={6}>
            <Card style={{height: '100%'}}>
              <Statistic
                  title='Е-баллы'
                  value={ebal}
                  prefix={<DollarCircleOutlined />}
                  groupSeparator={' '}
              />
            </Card>
          </Col>

        </Row>
      </div>
  );
}