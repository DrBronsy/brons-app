import * as React from 'react';
import {Space} from 'antd';

import {block as bem} from 'bem-cn';

import ProfileMainInfo from './ProfileMainInfo'
import VKDashboard from './VKDashboard'

import './index.scss';

const block = bem('profile-screen');

export default function Profile(): JSX.Element {
  return (
      <div className={block()}>
        <ProfileMainInfo className={block('profile-main-info')}/>
        <VKDashboard className={block('vk-dashboard')}/>
      </div>
  );
}