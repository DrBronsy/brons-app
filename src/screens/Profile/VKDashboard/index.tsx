import * as React from 'react';

import {Card, Col, Row, Space, Alert, Button, Statistic} from 'antd';
import {LinkOutlined} from '@ant-design/icons';
import {block as bem} from 'bem-cn';

import './index.scss';
import useGetVkInfo from 'hooks/useGetVkInfo';
import useGetScore from 'hooks/useGetScore';

import {VkCommentIcon, VkIcon, VkLikeIcon, VkRepostIcon} from 'components/Icons';

const block = bem('vk-dashboard');

export interface Props {
  className?: string
}

export default function ProfileProfileMainInfo({className}: Props): JSX.Element {
  const {loading: vkInfoLoading, data: vkInfoData} = useGetVkInfo();
  const {loading: scoreLoading, data: scoreData} = useGetScore();

  if (scoreLoading || vkInfoLoading) return <div>'Loading...'</div>;

  const {user: userScores} = scoreData;
  const {scores} = userScores;
  const {vk: {likes, comments, repost}} = scores;
  const {user} = vkInfoData;
  const {connections} = user;
  const {vk} = connections;
  const isConnections = !!vk;
  const isMember = !!vk?.isMember

  return (<div className={block()}>
    <Card
        className={block('control-panel')}
        bordered={false}
        bodyStyle={{width: '100%', marginBottom: '14px'}}
    >
      <Space direction='vertical' size={20}>
        {isConnections && !isMember && <Alert
            message='Внимание!'
            description='Для того, что закончить начальную верификацию,
                  участвовать в конкурсах и получать баллы за активность необходимо подписаться на наше сообщество в VK'
            type='warning'
            showIcon={true}
        />}

        <Space direction='horizontal'>
          <Button
              href='/login/vk'
              type='primary'
              className={block('vk-connect-btn')}
              icon={<VkIcon style={{color: '#fff'}} />}
              size='large'
              disabled={isConnections}
          >
            Подключиться
          </Button>

          {isConnections && <Button
              href='https://vk.com/bronsysq'
              target='_blank'
              type='primary'
              className={block('vk-connect-btn')}
              icon={<LinkOutlined twoToneColor='#fff'/>}
              size='large'
              disabled={isMember}
          >
            Подписаться
          </Button>}
        </Space>
      </Space>
    </Card>

    <Row gutter={14}>
      <Col span={8}>
        <Card>
          <Statistic
              title='Лайки'
              value={likes}
              prefix={<VkLikeIcon />}
              groupSeparator={' '}
          />
        </Card>
      </Col>
      <Col span={8}>
        <Card>
          <Statistic
              title='Репосты'
              value={repost}
              prefix={<VkRepostIcon />}
              groupSeparator={' '}
          />
        </Card>
      </Col>
      <Col span={8}>
        <Card>
          <Statistic
              title='Комментарии'
              value={comments}
              prefix={<VkCommentIcon />}
              groupSeparator={' '}
          />
        </Card>
      </Col>
    </Row>
  </div>);
}