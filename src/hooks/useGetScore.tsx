import {gql, useQuery} from '@apollo/client';

const ME_ID = gql`
    query Session {
        session {
            userId
        }
    }
`

const USER_SCORE = gql`
    query User($userId: String!) {
        user(userId: $userId) {
            scores {
                ebal
                vk {
                    likes
                    repost
                    comments
                }
            }
        }
    }
`

export default function useGetMe() {
  const {data: dataMe} = useQuery(ME_ID);
  return useQuery(USER_SCORE, {
    variables: {
      userId: dataMe.session.userId
    }
  })

}