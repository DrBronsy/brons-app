import {gql, useQuery} from '@apollo/client';

const ME_ID = gql`
    query Session {
        session {
            userId
        }
    }
`

const VK_INFO = gql`
    query User($userId: String!) {
        user(userId: $userId) {
            connections {
                vk {
                    id
                    isMember
                }
            }
        }
    }
`

export default function useGetMe() {
  const {data: dataMe} = useQuery(ME_ID);
  const {loading, data} = useQuery(VK_INFO, {
    variables: {
      userId: dataMe.session.userId
    }
  });

  return {
    loading,
    data
  }
}