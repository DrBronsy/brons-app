import {gql, useQuery} from '@apollo/client';

const ME_ID = gql`
    query Session {
        session {
            userId
        }
    }
`

const USER = gql`
    query User($userId: String!) {
        user(userId: $userId) {
            steam {
                avatarfull
                profileurl
                trustRating
                timecreated
            }
            csgoStat {
                playtime_forever
                total_time_played
                playtime_2weeks
            }
            displayName
            connections {
                vk {
                    id
                    isMember
                }
            }
        }
    }
`

export default function useGetMe() {
  const {data: dataMe} = useQuery(ME_ID);
  return useQuery(USER, {
    variables: {
      userId: dataMe.session.userId
    }
  })
}