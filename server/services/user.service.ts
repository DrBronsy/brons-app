import {UserSteam, UserVK} from 'models/user';
import {ObjectId} from 'mongodb';

import UserModel from '../models/user.model';
import {getCSGOMainStat, getOwnedGames, getSteamLevel} from 'services/steam.service';
import {checkUserJoinGroup} from 'services/vk.sevice';

export const addEBals = async (filter: object, value: number) => {
  await UserModel.updateOne(filter, {$inc: { 'scores.ebal': value }})
}

export const findUser = (filter: object) => {
  return UserModel.findOne(filter);
}

export const addVKProfile = async (id: ObjectId, profile: UserVK) => {
  const isMember = await checkUserJoinGroup(profile.id);

  if (isMember) {
    addEBals({_id: id}, 100);
  }

  return UserModel.findOneAndUpdate({_id: id}, {
    connections: {
      vk: {...profile, isMember}
    }
  });
}

export const createOrUpdateUserForSteam = async (profile: UserSteam) => {
  const userGames = await getOwnedGames(profile.id);
  const CSGOGamePreviewStat = userGames?.games && await userGames.games.find((game: any) => game.appid === 730)
  const CSGOGameMainStat = CSGOGamePreviewStat && await getCSGOMainStat(profile.id);
  const steamLevel =  await getSteamLevel(profile.id);

  const CSGOStat = {...CSGOGamePreviewStat, ...CSGOGameMainStat};

  const userUpdateInfo: object = {
    displayName: profile.displayName,
    steam: {lvl: steamLevel, ...profile._json},
    csgoStat: CSGOStat || {}
  }

  let user = await UserModel.findOneAndUpdate({'steam.steamid': profile.id}, userUpdateInfo)

  if (!user) {
    user =  await UserModel.create({
      ...userUpdateInfo,
      scores: {
        ebal: 1000
      },
    });
  }

  return user;
}