import VKApi from '../api/vk';
import UserModel from '../models/user.model';

export const checkUserJoinGroup = (userId: number) => {
  return VKApi.api.groups.isMember({group_id: '204401632', user_id: userId})
}

export const addUserLike = async (userId: number) => {
  await UserModel.updateOne({'connections.vk.id': userId}, {$inc: { 'scores.ebal': 10, 'scores.vk.likes': 1 }})
}

export const removeUserLike = async (userId: number) => {
  await UserModel.updateOne({'connections.vk.id': userId}, {$inc: { 'scores.ebal': -10, 'scores.vk.likes': -1 }})
}

export const leaveGroup = async (userId: number) => {
  await UserModel.updateOne({'connections.vk.id': userId}, {
    $set: { 'connections.vk.isMember': 0},
    $inc: { 'scores.ebal': -100 }
  })
}

export const joinGroup = async (userId: number) => {
  await UserModel.updateOne({'connections.vk.id': userId}, {
    $set: { 'connections.vk.isMember': 1},
    $inc: { 'scores.ebal': 100 }
  })
}

export const addCommentPost = async (userId: number) => {
  await UserModel.updateOne({'connections.vk.id': userId}, {$inc: { 'scores.ebal': 1, 'scores.vk.comments': 1 }})
}

export const removeCommentPost = async (userId: number) => {
  await UserModel.updateOne({'connections.vk.id': userId}, {$inc: { 'scores.ebal': -1, 'scores.vk.comments': -1 }})
}