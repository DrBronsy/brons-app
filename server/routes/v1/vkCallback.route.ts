import * as EXPRESS from 'express';
import config from 'config/app.config';
import {vkCallback} from 'controllers/vkCallback.controller';

const router = EXPRESS.Router();

router.use('/callback', (req, res, next) => {
  if (req.body.secret !== config.vk.clientCallbackSecret) {
    res.send(403).send('error: secret fail').end();
  }
  next();
});

router.post('/callback', vkCallback);

export default router;

