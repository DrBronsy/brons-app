import * as core from 'express-serve-static-core';
import { VK } from 'vk-io';
import CONFIG from '../config/app.config';

export default (APP: core.Express) => {

  APP.use('/vk/callback', (req, res) => {
    res.send(JSON.stringify(req.body))
  })

  APP.use('/vk/api', (req, res) => {

    const vk = new VK({
      token: 'c7ef8ae50e0f932360fdd2dd8e8dbf61f559adf52d6c0900331a9d6d388a5732d41054951c0f266a38d15'
    });

    async function run() {
      const response = await vk.api.groups.join({
        group_id: 204401632
      });

      console.log(response);
    }

    run().catch(console.log);
  })

}