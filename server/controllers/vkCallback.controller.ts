import {addUserLike, joinGroup, leaveGroup, removeUserLike, addCommentPost, removeCommentPost} from 'services/vk.sevice';

const handlers: any = {
    like_add: (data: {'object': {liker_id: number, object_type: string}}) => {
        if (data.object.object_type !== 'photo') {
            addUserLike(data.object.liker_id);
        }
    },
    like_remove: (data: {'object': {liker_id: number, object_type: string}}) => {
        if (data.object.object_type !== 'photo') {
            removeUserLike(data.object.liker_id);
        }
    },
    wall_reply_new: (data: {'object': {from_id: number}}) => {
        addCommentPost(data.object.from_id);
    },
    wall_reply_delete: (data: {'object': {deleter_id: number}}) => {
        removeCommentPost(data.object.deleter_id);
    },
    group_join: (data: {'object': {user_id: number}}) => {
        joinGroup(data.object.user_id);
    },
    group_leave: (data: {'object': {user_id: number}}) => {
        leaveGroup(data.object.user_id);
    }
}

export function vkCallback(req: any, res: any) {
    const handler = handlers[req.body.type];

    if (req.body.type === 'like_add' || req.body.type === 'like_remove') {
        console.info(new Date())
        console.info(req.body.object.liker_id)
        console.info(req.body)
        console.info(req.headers)
    }

    if (!handler) {
        res.status(404).send('error: not found: ' + req.body.type).end();
        return;
    } else {
        handler(req.body);
    }

    res.status(200).send('ok').end();
}