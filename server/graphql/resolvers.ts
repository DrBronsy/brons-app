import UserModel from '../models/user.model';
import {ObjectId} from 'mongodb';

export default (session?: any) => {
  return {
    Query: {
      async users() {
        const res: any = await UserModel.find({});
        return res.map((u: any) => ({
          id: u._id.toString(),
          displayName: u.displayName,
          steam: u.steam
        }));
      },
      async user(_: any, {userId}: { userId: ObjectId }) {
        const res: any = await UserModel.findOne({_id: userId});
        return res;
      },
      session() {
        return session
      }
    },
  };
}