import {gql} from 'apollo-server-express';

export default gql`
    type User {
        id: ID!
        displayName: String!
        steam: Steam!
        csgoStat: CSGOStat
        timecreated: Int
        connections: Connectoins
        scores: Scores
    }

    type Scores {
        ebal: Int
        vk: ScoreVk
    }

    type ScoreVk {
        likes: Int
        repost: Int
        comments: Int
    }

    type Steam {
        lvl: String
        steamid: String
        avatarfull: String
        profileurl: String
        trustRating: String
        timecreated: String
    }

    type Connectoins {
        vk: VKConnection
    }

    type VKConnection {
        id: String
        name: String
        isMember: Int
    }

    type Session {
        userId: String
        csrf: String!
        url: String
    }

    type CSGOStat {
        playtime_forever: Int
        total_time_played: Int
        playtime_2weeks: Int
    }

    type Query {
        users: [User]
        user(userId: String!): User!
        session: Session!
    }
`;