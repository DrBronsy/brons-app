import * as core from 'express-serve-static-core';

import {PassportStatic} from 'passport';

import {
  Strategy as VKStrategy,
  Params as VKParams,
  Profile as VKProfile,
} from 'passport-vkontakte';

import {DoneUser} from '../../src/models/user';

import CONFIG from '../../config/app.config';

import {redirect} from 'server/passport';

import {addVKProfile} from 'services/user.service';

export default (APP: core.Express, passport: PassportStatic) => {
  passport.use(
      new VKStrategy(
          {
            clientID: CONFIG.vk.clientID,
            clientSecret: CONFIG.vk.clientSecret,
            callbackURL: `${CONFIG.domain}/login/vk/callback`,
            passReqToCallback: true,
          } as any,
          ((
              req: core.Request,
              accessToken: string,
              refreshToken: string,
              params: VKParams,
              profile: VKProfile,
              done: DoneUser
          ) => {
            addVKProfile(req.user._id, {...profile, ...params})
            .then(() => done(null, req.user))
            .catch((error) => done(error));
          }) as any
      )
  );

  APP.get(
      '/login/vk',
      passport.authenticate('vkontakte')
  );

  APP.get(
      '/login/vk/callback',
      passport.authenticate('vkontakte'),
      redirect
  );
};
