import { VK } from 'vk-io';
import CONFIG from 'config/app.config';

const VKApi: VK = new VK({
  token: CONFIG.vk.serviceToken
});

export default VKApi;